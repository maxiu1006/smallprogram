import React, {Component} from 'react'
import {View, Image, Text} from '@tarojs/components'

import './index.scss'

/**
 * 个人中心页面
 * 优先展示缓存中的用户信息，再去网络请求中更新信息
 */
export default class Index extends Component {


  constructor(props) {
    super(props);


  }



  render() {
    const {appUserInfo} = this.state
    return (
      <View className='personally'>

        <View
          className='top-background main-color-background'
          style={`height: ${78 + this.globalData.outerHeight}px`}/>

        {
          (this.state.appUserInfo || false) ? (
            <View className='card-root'
                  style={`top: ${26 + this.globalData.outerHeight}px`}
            >

              <Text className='name lv1-text-color'></Text>
              <Text className='company lv1-text-color'></Text>

            </View>
          ) : null
        }

        <View className='menu-warp'>
          <View className='menu-item-root' onClick={this.onChangePasswordClick.bind(this)}>
            <Image className='menu-icon'
                   src='https://oss.yidayuntu.com/1561312770707528/20210617/16239091328644/icon／我的／列表／修改密码@2x.png'/>
            <Text className='menu-txt'>修改密码</Text>
            <Image class='arrow' src={commonIconMore}/>
          </View>
          <View className='item-line background'/>
          <View className='menu-item-root' onClick={this.onClearCacheClick.bind(this)}>
            <Image className='menu-icon'
                   src='https://oss.yidayuntu.com/1561312770707528/20210617/16239092123208/icon／我的／列表／清理缓存@2x.png'/>
            <Text className='menu-txt'>清理缓存</Text>
            <Image class='arrow' src={commonIconMore}/>

          </View>
          <View className='item-line background'/>
          <View className='menu-item-root' onClick={this.onAboutClick.bind(this)}>
            <Image className='menu-icon'
                   src='https://oss.yidayuntu.com/1561312770707528/20210617/16239092301372/icon／我的／列表／关于智园@2x.png'/>
            <Text className='menu-txt'>关于园羚</Text>
            <Image class='arrow' src={commonIconMore}/>

          </View>
          <View className='item-line background'/>
        </View>
        {/*<AtButton className='AtButton' type='secondary' onClick={this.onLoginOutClick.bind(this)}>退出登录</AtButton>*/}
        <View className='AtButton' onClick={this.onLoginOutClick.bind(this)}>退出登录</View>
      </View>
    )
  }
}
