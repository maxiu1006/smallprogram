import React, {Component} from 'react'
import {View, Text, ScrollView} from '@tarojs/components'
import './index.scss'



export default class Index extends Component {
  constructor() {
    super(...arguments)


  }


  render() {
    return (
      <View className='rent-chart-warp-root background'>
        <NavBar leftBack={false}
                title=''/>
        <View className='page-view'>
          <View className='rent-chart-content'>
            <ScrollView
              className='list-wrap'
              scroll-y
              refresherEnabled
              lowerThreshold={200}
              show-scrollbar={false}
              refresher-background='transparent'
             >
            </ScrollView>
          </View>
        </View>
      </View>
    )
  }
}
