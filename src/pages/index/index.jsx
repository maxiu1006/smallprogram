import React, {Component} from 'react'
import {View} from '@tarojs/components'
import Taro from '@tarojs/taro'
import {AtTabBar} from  'taro-ui'
import './index.scss'

import Home from './page/home/index'
import Rent from './page/rent-chart/index'
import Customer from './page/customer/customer-list'
import Personal from './page/personally/index'

import iconHome_n from '../../images/maxiugege.png'
import iconHome_s from '../../images/maxiugege.png'
import iconMine_n from '../../images/maxiugege.png'
import iconMine_s from '../../images/maxiugege.png'
import iconCustomer_s from '../../images/maxiugege.png'
import iconCustomer_n from '../../images/maxiugege.png'
import iconRent_n from '../../images/maxiugege.png'
import iconRent_s from '../../images/maxiugege.png'
export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  constructor() {
    super(...arguments)
    this.state = {
      currentPosition: 0,
      currentPage: Home,
      allPage: [Home, Rent, Customer, Personal]
    }
    this.handleClick(0, null, true)
  }

  handleClick(value, event, isFirst) {
    let frontColor = '#ffffff'
    if (value === 2 || value === 1) {
      frontColor = '#000000'
    }
    // 发送刷新首页数据的事件出去
    if (value === 0 && !isFirst) {
      Taro.eventCenter.trigger('mainHomeRefresh')
    }
    // 点击事件
    Taro.eventCenter.trigger('mainTabChangeEvent', {index: value})

    Taro.setNavigationBarColor({
      frontColor: frontColor,
      backgroundColor: '#FFFFFF',
      animation: {
        duration: 400,
        timingFunc: 'easeIn'
      }
    })
    this.setState({
      currentPosition: value,
      currentPage: this.state.allPage[value]
    })
  }

  render() {
    return (
      <View className='page'>

        {/*{this.state.currentPosition === 0 ? (<Home/>) : null}*/}
        {/*{this.state.currentPosition === 1 ? (<Rent/>) : null}*/}
        {/*{this.state.currentPosition === 2 ? (<Customer/>) : null}*/}
        {/*{this.state.currentPosition === 3 ? (<Personal/>) : null}*/}
        <View className='main-wrap' style={`display:${this.state.currentPosition === 0 ? 'visible' : 'none'}`}>
          <View/>
        </View>
        <View className='main-wrap' style={`display:${this.state.currentPosition === 1 ? 'visible' : 'none'}`}>
          <View/>
        </View>
        <View className='main-wrap' style={`display:${this.state.currentPosition === 2 ? 'visible' : 'none'}`}>

          <View/>
        </View>
        <View className='main-wrap' style={`display:${this.state.currentPosition === 3 ? 'visible' : 'none'}`}>
          <View/>
        </View>
        <View className='bottom-wrap' catchMove>
          <AtTabBar
            fixed
            tabList={[
              {
                title: '首页',
                selectedImage: iconHome_s,
                image: iconHome_n,
              },
              {
                title: '租控',
                image: iconRent_n,
                selectedImage: iconRent_s
              },
              {
                title: '客户',
                image: iconCustomer_n,
                selectedImage: iconCustomer_s
              },
              {
                title: '我的',
                image: iconMine_n,
                selectedImage: iconMine_s
              }
            ]}
            onClick={this.handleClick.bind(this)}
            current={this.state.currentPosition}
          />
        </View>
      </View>
    )
  }
}
