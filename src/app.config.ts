export default {
  pages: [
    'pages/index/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  subPackages: [
    {
      "root": "pages/customer/",
      "pages": [

      ],
      "title": '客户模块'
    },
    {
      "root": "pages/personally/",
      "pages": [

      ],
      "title": '个人模块',
    },
    {
      "root": "pages/main/",
      "pages": [
        "index",
      ],
      "title": '首页'
    },
  ],
  permission: {
    'scope.userLocation': {
      desc: '你的位置信息将用于小程序位置接口的效果展示'
    }
  },
  requiredBackgroundModes: ['audio']
}
